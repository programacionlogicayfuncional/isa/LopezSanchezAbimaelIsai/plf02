(ns plf02.core)
;Predicados
;1.associative?
(defn función-associative?-1
  [a]
  (associative? a))
(defn función-associative?-2
  [a]
  (associative? a))
(defn función-associative?-3
  [a]
  (associative? a))
(función-associative?-3 [])
(función-associative?-3 {})
(función-associative?-3 (list))
;2.boolean?
(defn función-boolean?-1
  [a]
  (boolean? a))
(defn función-boolean?-2
  [a]
  (boolean? a))
(defn función-boolean?-3
  [a]
  (boolean? a))
(función-boolean?-1 true)
(función-boolean?-1 false)
(función-boolean?-1 1)
;3.char?
(defn función-char?-1
  [a]
  (char? a))
(defn función-char?-2
  [a]
  (char? a))
(defn función-char?-3
  [a]
  (char? a))
(función-char?-1 \a)
(función-char?-2 \1)
(función-char?-3 "a")
;4.coll?
(defn función-coll?-1
  [a]
  (coll? a))
(defn función-coll?-2
  [a]
  (coll? a))
(defn función-coll?-3
  [a]
  (coll? a))
(función-coll?-1 [])
(función-coll?-2 "UwU")
(función-coll?-3 :a)
;5.decimal?
(defn función-decimal?-1
  [a]
  (decimal? a))
(defn función-decimal?-2
  [a]
  (decimal? a))
(defn función-decimal?-3
  [a]
  (decimal? a))
(función-decimal?-1 33M)
(función-decimal?-2 23.02)
(función-decimal?-3 1.0M)
;6.double?
(defn función-double?-1
  [a]
  (double? a))
(defn función-double?-2
  [a]
  (double? a))
(defn función-double?-3
  [a]
  (double? a))
(función-double?-1 33M)
(función-double?-2 23.02)
(función-double?-3 1.0M)
;7.float?
(defn función-float?-1
  [a]
  (float? a))
(defn función-float?-2
  [a]
  (float? a))
(defn función-float?-3
  [a]
  (float? a))
(función-float?-1 33M)
(función-float?-2 23.02)
(función-float?-3 1)
;8.ident?
(defn función-ident?-1
  [a]
  (ident? a))
(defn función-ident?-2
  [a]
  (ident? a))
(defn función-ident?-3
  [a]
  (ident? a))
(función-ident?-1 :x)
(función-ident?-2 "x")
(función-ident?-3 {:x "x"})
;9.indexed?
(defn función-indexed?-1
  [a]
  (indexed? a))
(defn función-indexed?-2
  [a]
  (indexed? a))
(defn función-indexed?-3
  [a]
  (indexed? a))
(función-indexed?-1 :x)
(función-indexed?-2 "x")
(función-indexed?-3 {:x "x"})
;10.int?
(defn función-int?-1
  [a]
  (int? a))
(defn función-int?-2
  [a]
  (int? a))
(defn función-int?-3
  [a]
  (int? a))
(función-int?-1 0)
(función-int?-2 180.0)
(función-int?-3 1M)
;11.integer?
(defn función-integer?-1
  [a]
  (integer? a))
(defn función-integer?-2
  [a]
  (integer? a))
(defn función-integer?-3
  [a]
  (integer? a))
(función-integer?-1 0)
(función-integer?-2 180.0)
(función-integer?-3 1N)
;12.keyword?
(defn función-keyword?-1
  [a]
  (keyword? a))
(defn función-keyword?-2
  [a]
  (keyword? a))
(defn función-keyword?-3
  [a]
  (keyword? a))
(función-keyword?-1 :x)
(función-keyword?-2 "x")
(función-keyword?-3 {:x "x"})
;13.list?
(defn función-list?-1
  [a]
  (list? a))
(defn función-list?-2
  [a]
  (list? a))
(defn función-list?-3
  [a]
  (list? a))
(función-list?-3 [1 2 3])
(función-list?-3 {:a "a"})
(función-list?-3 (list 1 2 3))
;14.map-entry?
(defn función-map-entry?-1
  [a]
  (map-entry? a))
(defn función-map-entry?-2
  [a]
  (map-entry? a))
(defn función-map-entry?-3
  [a]
  (map-entry? a))
(función-map-entry?-3 {:a 1 :b 2})
(función-map-entry?-3 (first {:a 1 :b 2}))
(función-map-entry?-3 :2)
;15.map?
(defn función-map?-1
  [a]
  (map? a))
(defn función-map?-2
  [a]
  (map? a))
(defn función-map?-3
  [a]
  (map? a))
(función-map?-3 {:a "a" :b "b"})
(función-map?-3 (first {:a \a :b \b}))
(función-map?-3 :1)
;16.nat-int?
(defn función-nat-int?-1
  [a]
  (nat-int? a))
(defn función-nat-int?-2
  [a]
  (nat-int? a))
(defn función-nat-int?-3
  [a]
  (nat-int? a))
(función-nat-int?-1 1)
(función-nat-int?-2 -1)
(función-nat-int?-3 1.0)
;17.number?
(defn función-number?-1
  [a]
  (number? a))
(defn función-number?-2
  [a]
  (number? a))
(defn función-number?-3
  [a]
  (number? a))
(función-number?-1 0)
(función-number?-2 180.0)
(función-number?-3 \1)
;18.pos-int?
(defn función-pos-int?-1
  [a]
  (pos-int? a))
(defn función-pos-int?-2
  [a]
  (pos-int? a))
(defn función-pos-int?-3
  [a]
  (pos-int? a))
(función-pos-int?-1 10)
(función-pos-int?-2 180.0)
(función-pos-int?-3 0)
;19.ratio?
(defn función-ratio?-1
  [a]
  (ratio? a))
(defn función-ratio?-2
  [a]
  (ratio? a))
(defn función-ratio?-3
  [a]
  (ratio? a))
(función-ratio?-1 7/5)
(función-ratio?-2 1.0)
(función-ratio?-3 10)
;20.rational?
(defn función-rational?-1
  [a]
  (rational? a))
(defn función-rational?-2
  [a]
  (rational? a))
(defn función-rational?-3
  [a]
  (rational? a))
(función-rational?-1 7/5)
(función-rational?-2 1.0)
(función-rational?-3 7)
;21.seq?
(defn función-seq?-1
  [a]
  (seq? a))
(defn función-seq?-2
  [a]
  (seq? a))
(defn función-seq?-3
  [a]
  (seq? a))
(función-seq?-1 (seq (list 1 2 3)))
(función-seq?-2 (seq [1 2 3]))
(función-seq?-3 (seq '(1 2 3)))
;22.seqable?
(defn función-seqable?-1
  [a]
  (seqable? a))
(defn función-seqable?-2
  [a]
  (seqable? a))
(defn función-seqable?-3
  [a]
  (seqable? a))
(función-seqable?-1 [])
(función-seqable?-2 "")
(función-seqable?-3 \.)
;23.sequential?
(defn función-sequential?-1
  [a]
  (sequential? a))
(defn función-sequential?-2
  [a]
  (sequential? a))
(defn función-sequential?-3
  [a]
  (sequential? a))
(función-sequential?-1 [])
(función-sequential?-2 "")
(función-sequential?-3 \.)
;24.set?
(defn función-set?-1
  [a]
  (set? a))
(defn función-set?-2
  [a]
  (set? a))
(defn función-set?-3
  [a]
  (set? a))
(función-set?-1 #{465 651 3 35 345 65})
(función-set?-2 [51 651 64 981])
(función-set?-3 {:1 1 :2 2})
;25.some?
(defn función-some?-1
  [a]
  (some? a))
(defn función-some?-2
  [a]
  (some? a))
(defn función-some?-3
  [a]
  (some? a))
(función-some?-1 #{465 651 3 35 345 65})
(función-some?-2 [51 651 64 981])
(función-some?-3 {:1 1 :2 2})
;26.string?
(defn función-string?-1
  [a]
  (string? a))
(defn función-string?-2
  [a]
  (string? a))
(defn función-string?-3
  [a]
  (string? a))
(función-string?-1 "holis")
(función-string?-2 ["4" "4"])
(función-string?-3 {:1 "1" :2 "2"})
;27.symbol?
(defn función-symbol?-1
  [a]
  (symbol? a))
(defn función-symbol?-2
  [a]
  (symbol? a))
(defn función-symbol?-3
  [a]
  (symbol? a))
(función-symbol?-1 'a)
(función-symbol?-2 \a)
(función-symbol?-3 "a")
;28.vector?
(defn función-vector?-1
  [a]
  (vector? a))
(defn función-vector?-2
  [a]
  (vector? a))
(defn función-vector?-3
  [a]
  (vector? a))
(función-vector?-1 [1 2 3])
(función-vector?-2 {:a \a :b \b})
(función-vector?-3 (list 1 2 3))
;Funciones de Orden Superior
;1.drop
(defn función-drop-1
  [a b]
  (drop a b))
(defn función-drop-2
  [a b]
  (drop a b))
(defn función-drop-3
  [a b]
  (drop a b))
(función-drop-1 0 [1 2 3])
(función-drop-2 1 [1 2 3])
(función-drop-3 2 [1 2 3])
;2.drop-last
(defn función-drop-last-1
  [a b]
  (drop-last a b))
(defn función-drop-last-2
  [a b]
  (drop-last a b))
(defn función-drop-last-3
  [a b]
  (drop-last a b))
(función-drop-last-1 0 [1 2 3])
(función-drop-last-2 1 [1 2 3])
(función-drop-last-3 2 [1 2 3])
;3.drop-while
(defn función-drop-while-1
  [a b]
  (drop-while a b))
(defn función-drop-while-2
  [a b]
  (drop-while a b))
(defn función-drop-while-3
  [a b]
  (drop-while a b))
(función-drop-while-1 neg? [-1 0 1])
(función-drop-while-2 pos? [-1 0 1])
(función-drop-while-3 zero? [-1 0 1])
;4.every?
(defn función-every?-1
  [a b]
  (every? a b))
(defn función-every?-2
  [a b]
  (every? a b))
(defn función-every?-3
  [a b]
  (every? a b))
(función-every?-1 {:a "a" :b "b"} [:a :b])
(función-every?-2 {:a "a" :b "b"} [:a :b :c])
(función-every?-3 {} [])
;5.filterv
(defn función-filterv-1
  [a b]
  (filterv a b))
(defn función-filterv-2
  [a b]
  (filterv a b))
(defn función-filterv-3
  [a b]
  (filterv a b))
(función-filterv-1 {:a "a" :b "b"} [:a :b])
(función-filterv-2 {:a "a" :b "b"} [:a :b :c])
(función-filterv-3 {} [])
;6.group-by
(defn función-group-by-1
  [a b]
  (group-by a b))
(defn función-group-by-2
  [a b]
  (group-by a b))
(defn función-group-by-3
  [a b]
  (group-by a b))
(función-group-by-1 {:a "a" :b "b"} [:a :b])
(función-group-by-2 count {:a "a" :b "b"})
(función-group-by-3 pos? [1 2 3])
;7.iterate
(defn función-iterate-1
  [a b c d]
  (a b (iterate c d)))
(defn función-iterate-2
  [a b c d]
  (a b (iterate c d)))
(defn función-iterate-3
  [a b c d]
  (a b (iterate c d)))
(función-iterate-1 take 6 inc 5)
(función-iterate-2 take 6 dec 5)
(función-iterate-3 take 5 max 5)
;8.keep
(defn función-keep-1
  [a b]
  (keep a b))
(defn función-keep-2
  [a b]
  (keep a b))
(defn función-keep-3
  [a b]
  (keep a b))
(función-keep-1 neg? [-1 0 1])
(función-keep-2 pos? #{-1 0 2})
(función-keep-3 zero? (list -1 0 1))
;9.keep-indexed
(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))
(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))
(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))
(función-keep-indexed-1 vector "Abimael")
(función-keep-indexed-2 hash-map "Abimael")
(función-keep-indexed-3 list "Abimael")
;10.map-indexed
(defn función-map-indexed-1
  [a b]
  (map-indexed a b))
(defn función-map-indexed-2
  [a b]
  (map-indexed a b))
(defn función-map-indexed-3
  [a b]
  (map-indexed a b))
(función-map-indexed-1 vector "Abimael")
(función-map-indexed-2 hash-map "Abimael")
(función-map-indexed-3 list "Abimael")
;12.mapv
(defn función-mapv-1
  [a b]
  (mapv a b))
(defn función-mapv-2
  [a b c]
  (mapv a b c))
(defn función-mapv-3
  [a b]
  (mapv a b))
(función-mapv-1 vector [1 2 3 4 5 6])
(función-mapv-2 list [1 2 3] [1 2 3])
(función-mapv-3 pos? [2 -6 8 10 -1 3])
;13.merge-with
(defn función-merge-with-1
  [a b c]
  (merge-with a b c))
(defn función-merge-with-2
  [a b c]
  (merge-with a b c))
(defn función-merge-with-3
  [a b c]
  (merge-with a b c))
(función-merge-with-1 into {:2 2} {:1 1})
(función-merge-with-2 into {:a #{1 2 3}, :b #{4 5 6}} {:a #{2 3 7 8}, :c #{1 2 3}})
(función-merge-with-3 merge {:x {:1 1}} {:x {:2 2}})
;14.not-any?
(defn función-not-any?-1
  [a b]
  (not-any? a b))
(defn función-not-any?-2
  [a b]
  (not-any? a b))
(defn función-not-any?-3
  [a b]
  (not-any? a b))
(función-not-any?-1 nil? [true false nil])
(función-not-any?-2 nil? [true false])
(función-not-any?-3 some? ["HOla" 007])
;15.not-every?
(defn función-not-every?-1
  [a b]
  (not-every? a b))
(defn función-not-every?-2
  [a b]
  (not-every? a b))
(defn función-not-every?-3
  [a b]
  (not-every? a b))
(función-not-every?-1 nil? [true false nil])
(función-not-every?-2 nil? [true false])
(función-not-every?-3 some? ["HOla" 007])
;16.partition-by
(defn función-partition-by-1
  [a b]
  (partition-by a b))
(defn función-partition-by-2
  [a b]
  (partition-by a b))
(defn función-partition-by-3
  [a b]
  (partition-by a b))
(función-partition-by-1 nil? [true false nil])
(función-partition-by-2 nil? [true false])
(función-partition-by-3 even? [1 1 1 2 2 3 3 4])
;17.reduce-kv
(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))
(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))
(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))
(función-reduce-kv-1 conj [] [[5 6 7] 1 2 3])
(función-reduce-kv-2 conj #{5 6 7} [:a :b :c]) 
(función-reduce-kv-3 concat #{} [])
;18.remove
(defn función-remove-1
  [a b]
  (remove a b))
(defn función-remove-2
  [a b]
  (remove a b))
(defn función-remove-3
  [a b]
  (remove a b))
(función-remove-1 pos? [1 -2 2 -1 3 7 0])
(función-remove-2 neg? [1 -2 2 -1 3 7 0])
(función-remove-3 nil? [nil true false])
;19.reverse
(defn función-reverse-1
  [a]
  (reverse a))
(defn función-reverse-2
  [a]
  (reverse a))
(defn función-reverse-3
  [a]
  (reverse a))
(función-reverse-1 [1 -2 2 -1 3 7 0])
(función-reverse-2 {:1 1 :2 2 :3 3 })
(función-reverse-3 [nil true false])
;20.some
(defn función-some-1
  [a b]
  (some a b))
(defn función-some-2
  [a b]
  (some a b))
(defn función-some-3
  [a b]
  (some a b))
(función-some-1 even? [-2 -1 0 1 2])
(función-some-2 pos? [-2 -1 0 1 2])
(función-some-3 true? [nil true false])
;21.sort-by
(defn función-sort-by-1
  [a b]
  (sort-by a b))
(defn función-sort-by-2
  [a b]
  (sort-by a b))
(defn función-sort-by-3
  [a b]
  (sort-by a b))
(función-sort-by-1 count ["abi" "ya" "a"])
(función-sort-by-2 first [[1 :a] [2 :b] [3 :c]])
(función-sort-by-3 first [[nil] [false] [true]])
;22.split-with
(defn función-split-with-1
  [a b]
  (split-with a b))
(defn función-split-with-2
  [a b]
  (split-with a b))
(defn función-split-with-3
  [a b]
  (split-with a b))
(función-split-with-1 odd? [1 2 3 4 5 6])
(función-split-with-2 #{:c} [:1 :2 :3 :4])
(función-split-with-3 (complement #{:e}) [:a :b :c :d])
;23.take
(defn función-take-1
  [a b]
  (take a b))
(defn función-take-2
  [a b]
  (take a b))
(defn función-take-3
  [a b]
  (take a b))
(función-take-1 3 [1 2 3 4 5 6])
(función-take-2 1 [:1 :2 :3 :4])
(función-take-3 0 [:a :b :c :d])
;24.take-last
(defn función-take-last-1
  [a b]
  (take-last a b))
(defn función-take-last-2
  [a b]
  (take-last a b))
(defn función-take-last-3
  [a b]
  (take-last a b))
(función-take-last-1 3 [1 2 3 4 5 6])
(función-take-last-2 1 [:1 :2 :3 :4])
(función-take-last-3 0 [:a :b :c :d])
;25.take-nth
(defn función-take-nth-1
  [a b]
  (take-nth a b))
(defn función-take-nth-2
  [a b]
  (take-nth a b))
(defn función-take-nth-3
  [a b]
  (take-nth a b))
(función-take-nth-1 3 [1 2 3 4 5 6])
(función-take-nth-2 1 [{:a "a"} {} {:b "b"}])
(función-take-nth-3 1 [1])
;26.take-while
(defn función-take-while-1
  [a b]
  (take-while a b))
(defn función-take-while-2
  [a b]
  (take-while a b))
(defn función-take-while-3
  [a b]
  (take-while a b))
(función-take-while-1 true? [false false])
(función-take-while-2 nil? [nil true false])
(función-take-while-3 neg? [-2 -1 0 1 2 3])
;27.update
(defn función-update-1
  [a b c]
  (update a b c))
(defn función-update-2
  [a b c]
  (update a b c))
(defn función-update-3
  [a b c]
  (update a b c))
(función-update-1 [1 2 3] 2 inc)
(función-update-2 [1 2 3] 0 dec)
(función-update-3 [[5] [5] 1] 2 inc)
;28.update-in
(defn función-update-in-1
  [a b c]
  (update-in a b c))
(defn función-update-in-2
  [a b c]
  (update-in a b c))
(defn función-update-in-3
  [a b c]
  (update-in a b c))
(función-update-in-1 {:name "James" :age 26} [:age] inc )
(función-update-in-2 {:x 2 :y 7} [:x] inc)
(función-update-in-3 {:life 3 :end "game"} [:life] dec)
